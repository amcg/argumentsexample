﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arguments
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"I got arguments: {String.Join(", ", args)}");
            Console.ReadLine();
        }
    }
}
